package com.startx.core.mvc.factory;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.startx.core.system.constants.Constants;
import com.startx.core.system.model.AccessTarget;

/**
 * 接入点工厂
 */
public class MappingFactory {
	
	/**
	 * log 工具
	 */
	private static final Logger Log = Logger.getLogger(MappingFactory.class);
	/**
	 * 添加接入点
	 */
	private static final Map<String,AccessTarget> ACCESS_POINT = new HashMap<String, AccessTarget>();
	
	public static void setAccessMapping(String name,AccessTarget value) {
		
		if(ACCESS_POINT.containsKey(name)) {
			throw new RuntimeException("AccessMapping[URI]定义重复，请检查："+name);
		}
		
		Log.info(name+Constants.UNDER_LINE+value.getType().name());
		
		ACCESS_POINT.put(name, value);
	}
	
	/**
	 * 获取接入点
	 * @param name
	 * @return
	 */
	public static AccessTarget getAccessTarget(String name) {
		return ACCESS_POINT.get(name);
	}
}
