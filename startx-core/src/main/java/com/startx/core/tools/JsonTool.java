package com.startx.core.tools;

import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class JsonTool {
	public static String obj2json(Object obj) {
		return JSON.toJSONString(obj,
				SerializerFeature.WriteNullStringAsEmpty,
				SerializerFeature.WriteNullNumberAsZero,
				SerializerFeature.WriteNullBooleanAsFalse,
				SerializerFeature.WriteNullListAsEmpty);
	}

	@SuppressWarnings("unchecked")
	public static <N extends Object>N json2obj(String json, Class<?> clz) {
		return (N) JSON.parseObject(json, clz);
	}
	
	public static Map<String,Object> json2map(String json) {
		return JSON.parseObject(json);
	}
}
