package com.startx.core.system.param;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.netty.handler.codec.http.multipart.MixedFileUpload;

/**
 * http表单参数
 */
public class HttpForm {
	/**
	 * 表单 参数
	 */
	private Map<String, List<String>> params;
	/**
	 * 文件参数
	 */
	private Map<String,List<MixedFileUpload>> uploads;
	
	/**
	 * 获取参数表
	 * @return
	 */
	public List<String> getParams(String key) {
		return params.get(key);
	}
	
	/**
	 * 获取参数
	 * @param key
	 * @return
	 */
	public String getParam(String key) {
		return params.get(key).get(0);
	}
	
	/**
	 * 获取文件参数
	 * @param key
	 * @return
	 */
	public MixedFileUpload getUpload(String key) {
		return uploads.get(key).get(0);
	}
	
	/**
	 * 获取文件参数
	 * @param key
	 * @return
	 */
	public List<MixedFileUpload> getUploads(String key) {
		return uploads.get(key);
	}
	
	/**
	 * 添加表单参数文件
	 * @param key
	 * @param upload
	 */
	public void param(String name,String value) {
		
		if(Objects.isNull(uploads)) {
			params = new HashMap<>();
		}
		
		if(params.containsKey(name)) {
			params.get(name).add(value);
		} else {
			List<String> values = new ArrayList<String>();
			values.add(value);
			params.put(name, values);
		}
	}
	
	/**
	 * 添加上传文件
	 * @param key
	 * @param upload
	 */
	public void upload(String key,MixedFileUpload upload) {
		
		if(Objects.isNull(uploads)) {
			uploads = new HashMap<>();
		}
		
		if(uploads.containsKey(key)) {
			uploads.get(key).add(upload);
		} else {
			List<MixedFileUpload> files = new ArrayList<MixedFileUpload>();
			files.add(upload);
			uploads.put(key, files);
		}
	}
}
