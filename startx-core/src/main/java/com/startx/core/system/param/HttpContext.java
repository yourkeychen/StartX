package com.startx.core.system.param;

import io.netty.channel.ChannelHandlerContext;

/**
 * HTTP上下文
 * 
 * @author minghu.zhang
 */
public class HttpContext {
	/**
	 * ip地址
	 */
	private String ip;
	/**
	 * 管道上下文
	 */
	private ChannelHandlerContext context;

	public HttpContext(String ip, ChannelHandlerContext context) {
		super();
		this.ip = ip;
		this.context = context;
	}

	public String getIp() {
		return ip;
	}

	public ChannelHandlerContext getContext() {
		return context;
	}

}
