package com.startx.core.system.config;

import com.startx.core.system.constants.Constants;

/**
 * 项目配置类
 */
public class StartxConfig {
	/**
	 * 绑定的域名
	 */
	private String ip = "localhost";
	/**
	 * 监听端口
	 */
	private int port = 8080;
	/**
	 * boss线程数
	 */
	private int boss = 2;
	/**
	 * workder线程数
	 */
	private int worker = 4;
	/**
	 * spring配置路径
	 */
	private String springPath = "classpath*:application.xml";
	/**
	 * 项目访问前缀
	 */
	private String endPoint = "/";
	/**
	 * websocket访问链接
	 */
	private String websocket = "/websocket";
	/**
	 * 是否为ssl
	 */
	private boolean isSSL = false;
	/**
	 * 静态资源目录
	 */
	private String webRoot = System.getProperty("user.home")+"/www";
	// 以下配置在ssl连接时使用
	/**
	 * jsk密码
	 */
	private String jksPwd;
	/**
	 * jsk路径
	 */
	private String jksPath;
	/**
	 * endpoint扫描路径
	 */
	private String packages = "com.startx";
	/**
	 * 设置NettyServer启动类
	 */
	private String server = "com.startx.core.netty.server.impl.HttpServer";
	/**
	 * 默认首页
	 */
	private String pageIndex = "/index.html";
	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getSpringPath() {
		return springPath;
	}

	public void setSpringPath(String springPath) {
		this.springPath = springPath;
	}

	public String getEndPoint() {
		
		if(endPoint.equals(Constants.ROOT_DIR)) {
			return Constants.EMPTY_STRING;
		}
		
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}

	public boolean isSSL() {
		return isSSL;
	}

	public void setSSL(boolean isSSL) {
		this.isSSL = isSSL;
	}

	public String getWebRoot() {
		return webRoot;
	}

	public void setWebRoot(String webRoot) {
		this.webRoot = webRoot;
	}

	public String getPackages() {
		return packages;
	}

	public void setPackages(String packages) {
		this.packages = packages;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public int getBoss() {
		return boss;
	}

	public void setBoss(int boss) {
		this.boss = boss;
	}

	public int getWorker() {
		return worker;
	}

	public void setWorker(int worker) {
		this.worker = worker;
	}

	public String getJksPwd() {
		return jksPwd;
	}

	public void setJksPwd(String jksPwd) {
		this.jksPwd = jksPwd;
	}

	public String getJksPath() {
		return jksPath;
	}

	public void setJksPath(String jksPath) {
		this.jksPath = jksPath;
	}

	public String getWebsocket() {
		return websocket;
	}

	public void setWebsocket(String websocket) {
		this.websocket = websocket;
	}

	public String getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(String pageIndex) {
		this.pageIndex = pageIndex;
	}

}
