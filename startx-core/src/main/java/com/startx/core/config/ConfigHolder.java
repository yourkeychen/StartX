package com.startx.core.config;

import java.io.File;
import java.util.Objects;

import com.startx.core.system.config.StartxConfig;
import com.startx.core.tools.ConfigTool;

public class ConfigHolder {

	private static StartxConfig config;
	
	/**
	 * 获取配置对象
	 */
	public static StartxConfig getConfig() {
		
		if(Objects.isNull(config)) {
			throw new RuntimeException("配置尚未初始化");
		}
		
		return config;
	}
	
	/**
	 * 执行配置
	 */
	public static void startConfig() {
		config = new ConfigTool().read("/config.properties", StartxConfig.class);
		
		//初始化目录
		File dir = new File(config.getWebRoot());
		if(!dir.exists()) {
			dir.mkdirs();
		}
	}
}
