package com.startx.core.netty.output.http;

import java.io.UnsupportedEncodingException;

import com.startx.core.system.constants.Headers;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;

/**
 * http短连接数据返回
 */
public class Resource {

	/**
	 * 返回http状态
	 * 
	 * @param ctx
	 * @param status
	 */
	public static void write(ChannelHandlerContext ctx, HttpResponseStatus status, HttpHeaders... headers) {
		FullHttpResponse res = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, status);
		setPublicHeader(res, headers);
		ChannelFuture f = ctx.channel().writeAndFlush(res);
		f.addListener(ChannelFutureListener.CLOSE);
	}

	/**
	 * 返回字节数组
	 * 
	 * @param ctx
	 * @param status
	 * @param data
	 */
	public static void write(ChannelHandlerContext ctx, HttpResponseStatus status, byte[] data,
			HttpHeaders... headers) {
		ByteBuf buffer = Unpooled.wrappedBuffer(data);
		FullHttpResponse res = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, status, buffer);
		setPublicHeader(res, headers);
		ChannelFuture f = ctx.channel().writeAndFlush(res);
		f.addListener(ChannelFutureListener.CLOSE);
	}

	/**
	 * 返回字符串
	 * 
	 * @param ctx
	 * @param status
	 * @param result
	 */
	public static void write(ChannelHandlerContext ctx, HttpResponseStatus status, String result,
			HttpHeaders... headers) throws UnsupportedEncodingException {
		ByteBuf buffer = Unpooled.wrappedBuffer(result.getBytes("utf-8"));
		FullHttpResponse res = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, status, buffer);
		setPublicHeader(res, headers);
		ChannelFuture f = ctx.channel().writeAndFlush(res);
		f.addListener(ChannelFutureListener.CLOSE);
	}

	/**
	 * 返回htt报文
	 */
	public static void write(ChannelHandlerContext ctx, FullHttpResponse res, HttpHeaders... headers) {
		setPublicHeader(res, headers);
		ChannelFuture f = ctx.channel().writeAndFlush(res);
		f.addListener(ChannelFutureListener.CLOSE);
	}

	/**
	 * 设置公共header
	 * 
	 * @param res
	 */
	private static void setPublicHeader(FullHttpResponse res, HttpHeaders... headers) {
		res.headers().add(Headers.getResourceHeader());
		for (HttpHeaders header : headers) {
			res.headers().add(header);
		}
	}
}
