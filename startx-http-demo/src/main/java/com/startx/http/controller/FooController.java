package com.startx.http.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.stereotype.Controller;

import com.startx.core.mvc.define.BodyType;
import com.startx.core.mvc.define.RequestMapping;
import com.startx.core.mvc.define.RequestMethod;
import com.startx.core.system.param.HttpBody;
import com.startx.core.system.param.HttpContext;
import com.startx.core.system.param.HttpHeader;
import com.startx.core.system.param.HttpParam;

import io.netty.channel.ChannelHandlerContext;

@Controller
@RequestMapping("/foo")
public class FooController {
	
	
	@RequestMapping(value="/bar",method=RequestMethod.GET,type=BodyType.JSON)
	public Object bar(ChannelHandlerContext ctx,
					  HttpContext context,
					  HttpParam param,
					  HttpBody body,
					  HttpHeader header) throws Exception {
		
		System.out.println(context.getRemoteAddress());
		System.out.println(param.getParams());
		System.out.println(body.map(BodyType.JSON));
		System.out.println(header.getHeaders());
		
		Map<String,Object> values = new HashMap<>();
		values.put("timestamp", System.currentTimeMillis());
		values.put("nonstr", UUID.randomUUID().toString());
		
		return values;
	}
	
	@RequestMapping(value="/rab",method=RequestMethod.GET,type=BodyType.XML)
	public Object barbar(ChannelHandlerContext ctx,
					  HttpContext context,
					  HttpParam param,
					  HttpBody body,
					  HttpHeader header) throws Exception {
		
		System.out.println(context.getRemoteAddress());
		System.out.println(param.getParams());
		System.out.println(body.map(BodyType.XML));
		System.out.println(header.getHeaders());
		
		Map<String,Object> values = new HashMap<>();
		values.put("timestamp", System.currentTimeMillis());
		values.put("nonstr", UUID.randomUUID().toString());
		
		return values;
	}
}
